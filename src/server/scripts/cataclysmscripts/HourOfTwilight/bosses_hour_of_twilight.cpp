#include "ScriptedCreature.h"
#include "hour_of_twilight.h"
#include "ScriptMgr.h"
#include "Unit.h"

/*
Cosa manca: 
Manca il target su Thrall da parte del boss per castargli L'icy tomb
Manca l'id dell'aura del chains of frost, non lo riesco a trovare
Dubbi:
Se ora metto un AI su thrall, quando muore il boss thrall rimane senza AI? Ogni boss deve cambiare quello che fa?
allora forse devo fare un unica AI dove faccio il check del boss che ho contro, in base a cosa ritorna
fa una determinata cosa.. Magari mi sbaglio
*/

enum spells
{
	HAND_OF_FROST = 102593,
	CHAINS_OF_FROST = 102582,
	ICY_BOULDER = 102198,
	ICY_TOMB = 103252, //immobilizza thrall in una tomba di ghiaccio, che deve essere distrutta da un player
	TORRENT_OF_FROST = 103962,
	THRALL_LAVA_BURST = 74020,
};

class boss_arcurion : public CreatureScript
{
public:
	boss_arcurion() : CreatureScript("boss_arcurion") { }
	CreatureAI* GetAI(Creature *Creature) const
	{
		return new boss_arcurionAI(Creature);
	}

	struct boss_arcurionAI : public BossAI
	{
		boss_arcurionAI(Creature* creature) : BossAI(creature, DATA_ARCURION)
		{

		}
		uint32 timer_hand_of_frost;
		uint32 timer_chain_of_frost;
		uint32 timer_summon_minion;
		//uint32 timer_icy_tomb = 1 * IN_MILLISECONDS;
		

		void Reset()
		{
			timer_hand_of_frost = 6 * IN_MILLISECONDS;
			timer_chain_of_frost = 9 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* who) //chiamato solo 1 volta quando inizia il fight
		{

		}

		void EnterEvadeMode()
		{

		}

		void ReceiveEmote(Player* /*player*/, uint32 uiTextEmote) 
		{

		}

		void UpdateAI(uint32 uiDiff)
		{
			if (!UpdateVictim())
			return;
			//-----------------------------------------------------------------------------
			if (timer_hand_of_frost <= uiDiff)
			{
				DoCast(me->getVictim(), HAND_OF_FROST);
				timer_hand_of_frost = 7 * IN_MILLISECONDS;
			}
			else
				timer_hand_of_frost -= uiDiff;
			//-----------------------------------------------------------------------------
			if (timer_chain_of_frost <= uiDiff)
			{
				DoCast(CHAINS_OF_FROST);
				//castare l'aura sui nemici entro i 65yd, non trovo l'aura su wowhead
			}
			else
				timer_chain_of_frost -= uiDiff;
			//-----------------------------------------------------------------------------
			if (timer_summon_minion <= uiDiff)
			{
				me->SummonCreature(NPC_FROZEN_SERVITOR, 0, 0, 0, 0.0f);
				timer_summon_minion = 8 * IN_MILLISECONDS;
			}
			else
				timer_summon_minion <= uiDiff;
			//-----------------------------------------------------------------------------
			if (me->GetHealthPct() <= 30)
			{
				DoCast(TORRENT_OF_FROST);
			}
			
			DoMeleeAttackIfReady();
		}
	};
};


class npc_frozen_servitor : public CreatureScript
{
public:
	npc_frozen_servitor() : CreatureScript("npc_frozen_servitor") { }
	CreatureAI* GetAI(Creature *Creature) const
	{
		return new npc_frozen_servitorAI(Creature);
	}

	struct npc_frozen_servitorAI : public BossAI
	{
		npc_frozen_servitorAI(Creature* creature) : BossAI(creature, DATA_ARCURION)
		{

		}
		uint32 timer_icy_boulder;
		void Reset()
		{
			timer_icy_boulder = 2 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* who) //chiamato solo 1 volta quando inizia il fight
		{

		}

		void EnterEvadeMode()
		{

		}

		void ReceiveEmote(Player* /*player*/, uint32 uiTextEmote)
		{

		}

		void UpdateAI(uint32 uiDiff)
		{
			if (!UpdateVictim())
			return;

			if (timer_icy_boulder <= uiDiff)
			{
				if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
				{
					DoCast(target, ICY_BOULDER);
				}
			}

			DoMeleeAttackIfReady();
		}
	};
};


class npc_thrall_hot : public CreatureScript
{
public:
	npc_thrall_hot() : CreatureScript("npc_thrall_hot") { }
	CreatureAI* GetAI(Creature *Creature) const
	{
		return new npc_thrall_hotAI(Creature);
	}

	struct npc_thrall_hotAI : public BossAI
	{
		npc_thrall_hotAI(Creature* creature) : BossAI(creature, DATA_THRALL_HOT)
		{

		}
		uint32 timer_lava_burst;
		void Reset()
		{
			timer_lava_burst = 1 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* who) //chiamato solo 1 volta quando inizia il fight
		{

		}

		void EnterEvadeMode()
		{

		}

		void ReceiveEmote(Player* /*player*/, uint32 uiTextEmote)
		{

		}

		void UpdateAI(uint32 uiDiff)
		{
			if (!UpdateVictim())
			return;

			if (timer_lava_burst <= uiDiff)
			{
				//for (int i = 0; me->getVictim()->isDead(); i++)
					//DoCast(me->SetTarget(NPC_FROZEN_SERVITOR), THRALL_LAVA_BURST);

			}
			else
				timer_lava_burst -= uiDiff;

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_arcurion()
{
	new boss_arcurion();
	new npc_frozen_servitor();
	new npc_thrall_hot();
}