#ifndef HOT_H_
#define HOT_H_

enum creatureIds
{
	BOSS_ARCURION = 54590,
	BOSS_ASIRA = 54968,
	BOSS_BENEDICTUS = 54938,
	NPC_THRALL = 57946,

	NPC_FROZEN_SERVITOR = 54600,
};
enum GameObjects
{

};
enum DataTypes
{
	DATA_ARCURION = 1,
	DATA_ASIRA = 2,
	DATA_BENEDICTUS = 3,
	DATA_THRALL_HOT = 4,
};
#endif //HOT_H_