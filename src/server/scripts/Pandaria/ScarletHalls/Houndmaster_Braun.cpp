#include "ScriptPCH.h"
#include "CreatureAI.h"

enum spell
{
	SPELL_DEATH_BLOSSOM = 114242,
	SPELL_CALL_DOG = 114259,
	SPELL_PIERCING_THROW = 114004,
	SPELL_BLOODY_MESS = 114056,
	SPELL_BLOODY_RAGE = 116140,
	SPELL_DOG_ICE_TRAP = 59269,

	//------------------------------
	//----adds
	//----- starving hound
	SPELL_HUNGRY_DOG = 111582,
	//----- angry hound
	SPELL_BITE = 17253,
	SPELL_LOCK_JAW = 90327,
	SPELL_RABID = 53401,
	//----- commander lindon
	SPELL_SHIELDBREAKER = 125506, //spell hit npc
	SPELL_SHIELD_BREAKER = 125507, //remove 113436 from player and destroy the npc
	SPELL_STAGGERING_SHOT = 113642, //melee attack
	//----- master archer
	SPELL_PIERCING_ARROW = 113479,
	SPELL_BLEEDING_WOUND = 113855,
};

enum testi
{
	TEXT_ON_COMBAT_START = 0,
	TEXT_CAST_PIERCING_THROW = 3,
	TEXT_CAST_BLOODY_MESS = 2,
	TEXT_CAST_DEATH_BLOSSOM = 4,
	TEXT_SUMMON_DOGS = 1,
	TEXT_PCT_HEALTH = 7,
	TEXT_ON_PLAYER_DEATH = 5,
	TEXT_ON_DEATH = 8,
};

class boss_houndmaster_braun : public CreatureScript
{
public:
	boss_houndmaster_braun() : CreatureScript("boss_houndmaster_braun"){}

	CreatureAI* GetAI(Creature* pCreature) const
	{
		return new boss_houndmaster_braunAI(pCreature);
	}

	struct boss_houndmaster_braunAI : public ScriptedAI
	{
		boss_houndmaster_braunAI(Creature *c) : ScriptedAI(c) {}

		uint32 DeathBlossomTimer;
		uint32 CallDogTimer;
		uint32 PiercingThrowTimer;
		uint32 BloodyRageTimer;
		uint32 BloodyMessTimer;
		uint32 numCani;
		uint32 countBuff;
		uint32 countSayText;
		std::list<Player*> playerList;
		std::list<Player*> toAggroList;
		std::list<Creature*> factionList;

		void Reset()
		{
			//me->RemoveAllAuras();

			DeathBlossomTimer = urand(8000, 10000);
			PiercingThrowTimer = 13000;
			BloodyMessTimer = PiercingThrowTimer;
			numCani = 0;
			countBuff = 0;
			countSayText = 0;
		}

		void KilledUnit(Unit* /*victim*/)
		{
			Talk(TEXT_ON_PLAYER_DEATH);
		}

		void JustDied(Unit * /*victim*/)
		{
			Talk(TEXT_ON_DEATH);
			GetCreatureListWithEntryInGrid(factionList, me, 59299, 1000.0f);
			for (Creature* c : factionList)
			{
				c->setFaction(14);
			}
			factionList.clear();
		}

		void EnterCombat(Unit * /*who*/)
		{
			Talk(TEXT_ON_COMBAT_START);
		}

		void UpdateAI(const uint32 uiDiff)
		{
			if (!UpdateVictim())
				return;
			//in combat
			if (me->HasUnitState(UNIT_STATE_CASTING))
				return;
			//-------------------------------------------------------------------------

			if (DeathBlossomTimer <= uiDiff)
			{
				Unit *randomTarget = SelectTarget(SELECT_TARGET_RANDOM, 0);
				me->GetMotionMaster()->MoveJump(randomTarget->GetPositionX(), randomTarget->GetPositionY(), randomTarget->GetPositionZ(), 10, 10, 0);
				Talk(TEXT_CAST_DEATH_BLOSSOM);
				DoCastVictim(SPELL_DEATH_BLOSSOM);
				//if it do damage
				GetPlayerListInGrid(playerList, me, 5.0f);
				for (Player* p : playerList)
				{
					if (countSayText == 0)
					{
						Talk(TEXT_CAST_BLOODY_MESS);
						countSayText++;
					}
					DoCast(p, SPELL_BLOODY_MESS);
				}
				playerList.clear();
				countSayText = 0;

				DeathBlossomTimer = urand(7000, 9000);
				
			}
			else
				DeathBlossomTimer -= uiDiff;
			//--------------------------------------------------------------------------
			if (PiercingThrowTimer <= uiDiff)
			{
				if (Unit *pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0))
				{
					Talk(TEXT_CAST_PIERCING_THROW);
					DoCast(pTarget, SPELL_PIERCING_THROW);
					DoCast(pTarget, SPELL_BLOODY_MESS);
					Talk(TEXT_CAST_BLOODY_MESS);

					PiercingThrowTimer = 7000;
				}
			}
			else
				PiercingThrowTimer -= uiDiff;
			//-----------------------------------------------------------------------------
			//call dogs
			//if (CallDogTimer <= uiDiff)						
			//{
			if ((me->GetHealthPct() <= 90 && numCani == 0) || (me->GetHealthPct() <= 80 && numCani == 1) || (me->GetHealthPct() <= 70 && numCani == 2) || (me->GetHealthPct() <= 60 && numCani == 3))
			{
				Talk(TEXT_SUMMON_DOGS);
				//DoCast(SPELL_CALL_DOG);
				//me->SummonCreature(59309, me->GetPositionX() + 10, me->GetPositionY(), me->GetPositionZ(), me->GetOrientation());
				me->SummonCreature(59309, 1011.29f, 508.69f, 12.906f, 0.0f);
				numCani++;//this fix the number of dogs called every pct.
			}
			//}
			//else
			//CallDogTimer -= uiDiff;
			//-----------------------------------------------------------------------------
			//enrage
			//if (BloodyRageTimer <= uiDiff)
			//{
			if (me->GetHealthPct() <= 50 && countBuff == 0)
			{
				Talk(TEXT_PCT_HEALTH);
				DoCast(me, SPELL_BLOODY_RAGE);
				countBuff++;
			}
			//}
			//else
			//BloodyRageTimer -= uiDiff;

			//-----------------------------------------------------------------------------
			DoMeleeAttackIfReady();
		}
	};
};

class ObedientHound : public CreatureScript
{
public:
	ObedientHound() : CreatureScript("npc_ObedientHound"){}

	CreatureAI* GetAI(Creature* pCreature) const
	{
		return new ObedientHoundAI(pCreature);
	}

	struct ObedientHoundAI : public ScriptedAI
	{
		ObedientHoundAI(Creature *c) : ScriptedAI(c) {}
		uint32 DogIceTrap_Timer;

		void Reset()
		{
			me->setFaction(14);
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
			//me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
			DogIceTrap_Timer = 0; //creates the trap when it spawn
			if (me->FindNearestCreature(59303, 1000.0f, false) || me->FindNearestCreature(59303, 1000.0f, true))
				me->ForcedDespawn();
		}

		void UpdateAI(const uint32 uiDiff)
		{
			if (!me->getVictim())
			{
			}

			if (!UpdateVictim())
				return;

			if (DogIceTrap_Timer <= uiDiff)
			{
				if (!me->FindNearestCreature(59303, 1000.0f, true)->HealthBelowPct(50))
				{
					DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_DOG_ICE_TRAP);//� un morso in questo caso perch� la spell dice che non esiste
					//DogIceTrap_Timer = 60000; //cast a ice trap every minute  since the icetrap lasts 1 minute, go see here http://www.wowhead.com/spell=135382 
					DogIceTrap_Timer = 6 * IN_MILLISECONDS;
				}
				else
				{
					me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
					me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
					DoCast(me->FindNearestCreature(59303, 1000.0f, true), SPELL_DOG_ICE_TRAP);
					if (me->FindNearestCreature(59303, 1000.0f, false))
						me->ForcedDespawn();
				}
			}
			else
				DogIceTrap_Timer -= uiDiff;
			DoMeleeAttackIfReady();
		}
	};
};

class npc_starving_hound : public CreatureScript
{
public:
	npc_starving_hound() : CreatureScript("npc_starving_hound") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_starving_hound_AI(creature);
	}

	struct npc_starving_hound_AI : public ScriptedAI
	{
		npc_starving_hound_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;

		void Reset()
		{
			me->setFaction(14);
			me->FindNearestCreature(58898, 7.0f, true)->setFaction(14);
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			/*GetCreatureListWithEntryInGrid(creatureList, me, 58898, 7.0f);
			for (Creature* c : creatureList)
			{
			if (me->FindNearestCreature(58898, 7.0f, true)->HasAura(3))
			{
			DoCast(c, SPELL_HUNGRY_DOG, true);
			}
			}*/
			DoMeleeAttackIfReady();
		}
	};
};

class npc_angry_hound : public CreatureScript
{
public:
	npc_angry_hound() : CreatureScript("npc_angry_hound") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_angry_hound_AI(creature);
	}

	struct npc_angry_hound_AI : public ScriptedAI
	{
		npc_angry_hound_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		uint32 timer_bite;
		uint32 timer_jaw;

		void Reset()
		{
			me->setFaction(14);
			timer_jaw = 2 * IN_MILLISECONDS;
			timer_bite = 4 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* /*who*/)
		{
			DoCast(me, SPELL_RABID);
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_bite <= diff)
			{
				DoCast(me->getVictim(), SPELL_BITE);
				timer_bite = 4 * IN_MILLISECONDS;
			}
			else
				timer_bite -= diff;

			if (timer_jaw <= diff)
			{
				DoCast(me->getVictim(), SPELL_LOCK_JAW);
				timer_jaw = 4 * IN_MILLISECONDS;
			}
			else
				timer_jaw -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

class npc_commander_lindon : public CreatureScript
{
public:
	npc_commander_lindon() : CreatureScript("npc_commander_lindon") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_commander_lindon_AI(creature);
	}

	struct npc_commander_lindon_AI : public ScriptedAI
	{
		npc_commander_lindon_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_shieldbreaker;
		uint32 timer_shield_breaker;
		uint32 timer_staggering_shot;
		uint32 scudi;
		bool destroyed_all;

		void Reset()
		{
			me->setFaction(14);
			timer_shieldbreaker = 2.5 * IN_MILLISECONDS;
			destroyed_all = false;
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
		}

		void EnterCombat(Unit* /*who*/)
		{
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{


			GetPlayerListInGrid(playerList, me, 50.0f);
			GetCreatureListWithEntryInGrid(creatureList, me, 59163, 100.0f);
			if (me->FindNearestCreature(59163, 50.0f, true))
			{
				for (Player* p : playerList)
				{
					for (Creature* c : creatureList)
					{
						if (timer_shieldbreaker <= diff)
						{
							DoCast(c, SPELL_SHIELDBREAKER);
							timer_shieldbreaker = 2.5 * IN_MILLISECONDS;
						}
						else
							timer_shieldbreaker -= diff;
					}
					DoCast(p, SPELL_SHIELD_BREAKER);
				}
			}
			else
			{
				if (!destroyed_all)
				{
					timer_staggering_shot = 4 * IN_MILLISECONDS;
					me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
					destroyed_all = true;
				}
				if (timer_staggering_shot <= diff)
				{
					DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_STAGGERING_SHOT);
					timer_staggering_shot = 4 * IN_MILLISECONDS;
				}
				else
					timer_staggering_shot -= diff;

				DoMeleeAttackIfReady();
			}
		}
	};
};

class npc_master_archer : public CreatureScript
{
public:
	npc_master_archer() : CreatureScript("npc_master_archer") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_master_archer_AI(creature);
	}

	struct npc_master_archer_AI : public ScriptedAI
	{
		npc_master_archer_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_bleeding_wound;
		uint32 timer_piercing_arrow;
		bool destroyed_all;

		void Reset()
		{
			me->setFaction(14);
			timer_piercing_arrow = 2.5 * IN_MILLISECONDS;
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
		}

		void EnterCombat(Unit* /*who*/)
		{
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{


			GetPlayerListInGrid(playerList, me, 50.0f);
			GetCreatureListWithEntryInGrid(creatureList, me, 59163, 100.0f);
			if (me->FindNearestCreature(59163, 50.0f, true))
			{

				for (Player* p : playerList)
				{
					for (Creature* c : creatureList)
					{
						if (timer_piercing_arrow <= diff)
						{
							DoCast(c, SPELL_PIERCING_ARROW);
							timer_piercing_arrow = 2.5 * IN_MILLISECONDS;
						}
						else
							timer_piercing_arrow -= diff;
					}
				}
			}
			else
			{
				if (!destroyed_all)
				{
					timer_bleeding_wound = 4 * IN_MILLISECONDS;
					me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
					destroyed_all = true;
				}
				if (timer_bleeding_wound <= diff)
				{
					DoCast(me->getVictim(), SPELL_BLEEDING_WOUND);
					timer_bleeding_wound = 4 * IN_MILLISECONDS;
				}
				else
					timer_bleeding_wound -= diff;

				DoMeleeAttackIfReady();
			}
		}
	};
};

class npc_scarlet_guardian : public CreatureScript
{
public:
	npc_scarlet_guardian() : CreatureScript("npc_scarlet_guardian") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_guardian_AI(creature);
	}

	struct npc_scarlet_guardian_AI : public ScriptedAI
	{
		npc_scarlet_guardian_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;

		void Reset()
		{
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (me->getFaction() == 35)//don't permit to skip the boss to some1 that can use invisibility
			{
				GetPlayerListInGrid(playerList, me, 2.0f);
				for (Player* p : playerList)
				{
					if (!p->IsGameMaster())
						p->NearTeleportTo(me->GetPositionX()-10, me->GetPositionY(), me->GetPositionZ()+5, me->GetOrientation());
				}
				playerList.clear();
			}
			

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_HoundmasterBraun()
{
	new boss_houndmaster_braun();
	new ObedientHound();
	new npc_starving_hound();
	new npc_angry_hound();
	new npc_commander_lindon();
	new npc_master_archer();
	new npc_scarlet_guardian();
}