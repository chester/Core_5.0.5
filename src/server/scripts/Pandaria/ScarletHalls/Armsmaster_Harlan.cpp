#include "ScriptPCH.h"
#include "ScriptedEscortAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Unit.h"
#include "ScriptedFollowerAI.h"
#include "FollowerReference.h"


/*
Bug conosciuti:
dopo aver fatto il primo giro della stanza il boss rimane in mezzo.
se il boss non ti prende in lista mentre č al centro fa tutto il giro con l'effetto della trottola, perň se ti incontra per la strada il player non lo insegue
se il boss ti prende in lista mentre č al centro, va al prossimo waypoint e il server crasha
Working 90%
*/
enum spell
{
	SPELL_HEROIC_LEAP = 111218,
	SPELL_DRAGON_REACH = 111217,
	SPELL_BLADES_OF_LIGHT = 111216,
	SPELL_BERSERKER_RAGE = 111221,
	SPELL_CALL_FOR_HELP = 82137, // this should call a Scarlet Defender (id = 58998)
	SPELL_BLADES_OF_LIGHT_RANGE = 111394, //not sure about this, in wowhead it says "9yd range" and this is an aura, so i think it has to be casted on the boss, so player that are in 9yd range from the boss takes damage

	//----adds
	//--Scarlet Scourge Hewer
	SPELL_SEAL_OF_BLOOD = 111351,
	SPELL_INQUISITION = 111341,
	//--Scarlet Myrmidon
	SPELL_WHIRLWIND = 84147,
	SPELL_ENRAGE = 12880,
	//--Scarlet Evangelist
	SPELL_HOLY_FIRE = 128232,
	SPELL_FANATICISM = 111376,
	SPELL_FIERY_PURGE = 111580,
	SPELL_SURRENDER_HEALTH = 111398,
	//--Scarlet Cannoneer
	SPELL_BOMB = 125198,


};

enum testi
{
	TEXT_ENTER_COMBAT = 2,
	TEXT_ON_DEATH = 1,
	TEXT_ON_PLAYER_DEATH = 4,
	TEXT_PLAYER_PCT = 1,
	TEXT_BLADES_OF_LIGHT = 0,
};

struct Location
{
	uint32 wpId;
	float x;
	float y;
	float z;
};

static Location BossWP[] =
{
	{ 1, 1203.155f, 455.078f, 1.024f },
	{ 2, 1213.506f, 454.681f, 3.391f },
	{ 3, 1219.575f, 444.160f, 6.082f },
	{ 4, 1224.458f, 437.359f, 6.082f },
	{ 5, 1213.109f, 425.869f, 6.082f },
	{ 6, 1197.079f, 426.337f, 8.150f },
	{ 7, 1187.197f, 443.811f, 11.176f },
	{ 8, 1192.874f, 458.412f, 9.140f },
	{ 9, 1205.642f, 463.523f, 6.531f },
	{ 10, 1218.946f, 458.380f, 6.082f },
	{ 11, 1224.745f, 447.615f, 6.082f },
	{ 12, 1219.889f, 444.464f, 6.082f },
	{ 13, 1214.604f, 433.685f, 3.778f },
	{ 14, 1203.867f, 431.340f, 1.061f },
	{ 15, 1206.337f, 443.899f, 0.988f }
};

class Armsmaster_Harlan : public CreatureScript
{
public:
	Armsmaster_Harlan() : CreatureScript("boss_Armsmaster_Harlan"){}

	CreatureAI* GetAI(Creature* pCreature) const
	{
		return new Armsmaster_HarlanAI(pCreature);
	}

	struct Armsmaster_HarlanAI : public ScriptedAI
	{
		Armsmaster_HarlanAI(Creature *c) : ScriptedAI(c)
		{
			DefaultMoveSpeedRate = c->GetSpeedRate(MOVE_WALK);
		}

		uint32 HeroicLeapTimer;
		uint32 DragonReachTimer;
		uint32 BladesOfLightRangeTimer;
		uint32 BladesOfLightTimer;
		uint32 BerserkerRageTimer;
		uint32 CallForHelpTimer;
		uint32 muovinpc;
		uint32 waypId;
		float DefaultMoveSpeedRate;
		bool trottola;
		bool isCentroStanza;
		bool moved;
		bool center;
		bool isArrived;
		bool isCasting;
		bool jumped;
		bool isCentroStanza2;
		uint32 moveCount;
		uint32 timer_spell_trottola;
		uint32 timer_danno_trottola;
		uint32 timer_effetto_trottola;
		uint32 timer_heroic_leap;
		uint32 haCastato;
		uint32 contatoreTrottolaPlayer;
		std::list<Player*> playerList;
		std::list<Player*> playerListRem;

		void Reset()
		{
			me->RemoveAllAuras();
			contatoreTrottolaPlayer = 0;
			DragonReachTimer = 6000;
			CallForHelpTimer = 15000;
			HeroicLeapTimer = urand(10000, 14000);
			BladesOfLightTimer = HeroicLeapTimer + 500;
			muovinpc = 0;
			waypId = 0;
			trottola = false;
			isCentroStanza = false;
			moved = false;
			isArrived = false;
			center = false;
			isCasting = false;
			jumped = false;
			isCentroStanza2 = false;
			moveCount = 0;
			haCastato = 0;
			timer_spell_trottola = 500;
			timer_danno_trottola = 0;
			timer_effetto_trottola = 0;
			timer_heroic_leap = 0;
			playerList.clear();
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_SERVER_CONTROLLED);
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);//dopo aver tolto queste 3 flag, il boss č nuovamente attaccabile

		}

		void KilledUnit(Unit * /*victim*/)
		{
			Talk(TEXT_ON_PLAYER_DEATH);
		}

		void JustDied(Unit * /*victim*/)
		{
			Talk(TEXT_ON_DEATH);
		}

		void EnterCombat(Unit * /*who*/)
		{
			Talk(TEXT_ENTER_COMBAT);
		}


		void SpellHitTarget(Unit* target, SpellInfo const* spell)
		{
			if (spell->Id != 111215)
				return;

			for (Player* player : playerList)
			{
				if (player->GetGUID() == target->GetGUID())
				{
					playerList.remove(player);
					break;
				}
			}
		}

		void UpdateAI(const uint32 uiDiff)
		{
			if (!me->getVictim()) //out of combat
			{
			}

			if (!UpdateVictim())
				return;
			//in combat
			//if (me->getVictim()->GetHealthPct() < 50)
				//Talk(TEXT_PLAYER_PCT);
			//-----------------------------------------------------------------------------
			if (HeroicLeapTimer <= uiDiff)
			{
				trottola = true;
				if (moveCount == 0 && (!isCentroStanza))
				{
					if (!isArrived)
					{
						me->GetMotionMaster()->MoveJump(1206.337, 443.899, 0.988, 10, 10);
						isArrived = true;
						jumped = true;

					}
					if (jumped)
						if ((uint32)me->GetPositionX() == 1206 && (uint32)me->GetPositionY() == 443 && (uint32)me->GetPositionZ() == 0)
						{
						DoCast(SPELL_HEROIC_LEAP);
						center = true;
						jumped = false;
						//isCentroStanza2 = false;
						}

					if (center)
					{
						if (!isCasting) {
							me->MonsterYell(TEXT_BLADES_OF_LIGHT, LANG_UNIVERSAL, 0);
							me->CastSpell(me, 111216, true);
							isCasting = true;
						}

						if (isCasting)
						{
							me->SetSpeed(MOVE_WALK, 20.0f, true);
							//me->UpdateSpeed(MOVE_WALK, true);
							timer_spell_trottola = 500;
							me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_SERVER_CONTROLLED);
							me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
							me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
							center = false;
						}
					}
				}
				if (isCasting /*&& me->HasUnitState(UNIT_STATE_CASTING)*/)
				{
					isCentroStanza = true;
					isCasting = false;
				}

				if (isCentroStanza) {
					if (timer_spell_trottola <= uiDiff) {

						if (!moved) {
							me->GetMotionMaster()->MovePoint(BossWP[moveCount].wpId, BossWP[moveCount].x, BossWP[moveCount].y, BossWP[moveCount].z);
							moved = true;
						}

						if ((uint32)me->GetPositionX() == (uint32)BossWP[moveCount].x && (uint32)me->GetPositionY() == (uint32)BossWP[moveCount].y &&
							(uint32)me->GetPositionZ() == (uint32)BossWP[moveCount].z) {
							moved = false;
							if (BossWP[moveCount].wpId == 15)//se č arrivato al 15° waypoint, esce dal ciclo.
							{
								moveCount = 0; //resetta il waypoint per essere percorso nuovamente
								isCentroStanza = false;//test
								isArrived = false;//test							
								trottola = false;//puň ora eseguire altre spell
								me->SetSpeed(MOVE_WALK, DefaultMoveSpeedRate);//setta velocitŕ normale
								me->RemoveAura(111216);//toglie l'effetto della trottola
								me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_SERVER_CONTROLLED);
								me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
								me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);//dopo aver tolto queste 3 flag, il boss č nuovamente attaccabile
								GetPlayerListInGrid(playerListRem, me, 6.0f);//prende la lista di chi ha catturato con la trottola
								for (Player* player : playerListRem)
								{
									player->SetSpeed(MOVE_WALK, DefaultMoveSpeedRate);//setta la velocitŕ normale ai player
									player->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);//ora i player possono muoversi come vogliono
									playerListRem.remove(player);//rimuove i player dalla lista 1 alla volta
								}
								HeroicLeapTimer = urand(20000, 25000);//prossima trottola tra 20 e 25 secondi
								me->GetMotionMaster()->MoveChase(SelectTarget(SELECT_TARGET_TOPAGGRO, 0));
							}
							else //altrimenti va al prossimo waypoint
								moveCount++;
						}

						if (timer_danno_trottola <= uiDiff)
						{

							//se č qua dentro significa che il boss č al centro della stanza e inizia il primo waypoint

							GetPlayerListInGrid(playerList, me, 6.0f);


							for (Player* player : playerList)
							{


								me->CastSpell(player, 111215, true);
								//player->GetMotionMaster()->MoveFollow(me, 0.0f, 0, MOTION_SLOT_ACTIVE);//il player segue il boss, cosa che fa
								DoTeleportPlayer(player, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation());
								//player->SetSpeed(MOVE_WALK, 25.0f, true);//aumenta la velocitŕ, ma gli add del boss potrebbero rallentare, il player deve essere più veloce per non essere seminato dal boss
								//player->UpdateSpeed(MOVE_WALK, true);
								//player->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
							}
							playerList.clear();
							timer_danno_trottola = 75;
						}
						else
							timer_danno_trottola -= uiDiff;


					}
					else
						timer_spell_trottola -= uiDiff;

				}
			}
			else
				HeroicLeapTimer -= uiDiff;

			//----------------------------------------------------------------------------

			if (!trottola){
				if (DragonReachTimer <= uiDiff)
				{
					DoCast(me->getVictim(), SPELL_DRAGON_REACH);//80% weapon damage to all enemies within 8 yards in front of the caster.

					DragonReachTimer = 8000;
				}
				else
					DragonReachTimer -= uiDiff;
				//-----------------------------------------------------------------------------
				if (CallForHelpTimer <= uiDiff)
				{
					DoCast(SPELL_CALL_FOR_HELP); //this should spawn 2 Scarlet Defender
					me->SummonCreature(58998, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 0.0f);
					me->SummonCreature(58998, me->GetPositionX() + 10, me->GetPositionY() + 10, me->GetPositionZ(), 0.0f);
					CallForHelpTimer = 14000;
				}
				else
					CallForHelpTimer -= uiDiff;
				//-----------------------------------------------------------------------------
				DoMeleeAttackIfReady();
			}
		}
	};
};

class ScarletDefender : public CreatureScript
{
public:
	ScarletDefender() : CreatureScript("ScarletDefender"){}

	CreatureAI* GetAI_ScarletDefender(Creature* pCreature) const
	{
		return new ScarletDefenderAI(pCreature);
	}

	struct ScarletDefenderAI : public ScriptedAI
	{
		ScarletDefenderAI(Creature *c) : ScriptedAI(c) {}
		uint32 HeavyArmor_Timer;

		void Reset()
		{
			//don't know how to delete the summoned Scarlet Defender if combat is reseted
			HeavyArmor_Timer = 0; //add armor buff when spawned, they take 20% less damage
			//When the armor is destroyed they take 200% more damage
		}

		void UpdateAI(const uint32 uiDiff)
		{
			if (!me->getVictim())
			{
			}

			if (!UpdateVictim())
				return;

			DoCast(me, 111323);//armatura per prendere meno danni



			DoMeleeAttackIfReady();
		}
	};
};

class npc_scarlet_scourge_hewer : public CreatureScript
{
public:
	npc_scarlet_scourge_hewer() : CreatureScript("npc_scarlet_scourge_hewer") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_scourge_hewer_AI(creature);
	}

	struct npc_scarlet_scourge_hewer_AI : public ScriptedAI
	{
		npc_scarlet_scourge_hewer_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_inquisition;

		void Reset()
		{
			me->setFaction(14);
			timer_inquisition = 2 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* /*who*/)
		{
			DoCast(me, SPELL_SEAL_OF_BLOOD);
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_inquisition <= diff)
			{
				DoCast(me, SPELL_INQUISITION);
				timer_inquisition = 10 * IN_MILLISECONDS;
			}
			else
				timer_inquisition -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

class npc_scarlet_myrmidon : public CreatureScript
{
public:
	npc_scarlet_myrmidon() : CreatureScript("npc_scarlet_myrmidon") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_myrmidon_AI(creature);
	}

	struct npc_scarlet_myrmidon_AI : public ScriptedAI
	{
		npc_scarlet_myrmidon_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_whirlwind;
		uint32 timer_enrage;

		void Reset()
		{
			me->setFaction(14);
			timer_enrage = 0;
			timer_whirlwind = 5 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_whirlwind <= diff)
			{
				DoCast(SPELL_WHIRLWIND);
				timer_whirlwind = 5 * IN_MILLISECONDS;
			}
			else
				timer_whirlwind -= diff;

			if (timer_enrage <= diff)
			{
				DoCast(me, SPELL_ENRAGE);
				timer_enrage = 15.1 * IN_MILLISECONDS;
			}
			else
				timer_enrage -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

class npc_scarlet_evangelist : public CreatureScript
{
public:
	npc_scarlet_evangelist() : CreatureScript("npc_scarlet_evangelist") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_evangelist_AI(creature);
	}

	struct npc_scarlet_evangelist_AI : public ScriptedAI
	{
		npc_scarlet_evangelist_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_fiery_purge;
		uint32 timer_holy_fire;
		uint32 timer_cura;

		void Reset()
		{
			me->setFaction(14);
			timer_fiery_purge = 2 * IN_MILLISECONDS;;
			timer_holy_fire = 5 * IN_MILLISECONDS;
			timer_cura = 8 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* /*who*/)
		{
			DoCast(me, SPELL_FANATICISM);
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_holy_fire <= diff)
			{
				DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_HOLY_FIRE);
				timer_holy_fire = 15 * IN_MILLISECONDS;
			}
			else
				timer_holy_fire -= diff;

			if (timer_fiery_purge <= diff)
			{
				DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_FIERY_PURGE);
				timer_fiery_purge = 13 * IN_MILLISECONDS;
			}
			else
				timer_fiery_purge -= diff;

			/*if (timer_cura <= diff)
			{
				if (me->FindNearestCreature(58684, 10.0f, true)->GetHealthPct() <= 50)
				{
					DoCast(me->FindNearestCreature(58684, 10.0f, true), SPELL_SURRENDER_HEALTH);
				}
				if (me->FindNearestCreature(58683, 10.0f, true)->GetHealthPct() <= 50)
				{
					DoCast(me->FindNearestCreature(58683, 10.0f, true), SPELL_SURRENDER_HEALTH);
				}
				timer_cura = 11 * IN_MILLISECONDS;
			}
			else
				timer_cura -= diff;*/

			DoMeleeAttackIfReady();
		}
	};
};

class npc_scarlet_cannoneer : public CreatureScript
{
public:
	npc_scarlet_cannoneer() : CreatureScript("npc_scarlet_cannoneer") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_cannoneer_AI(creature);
	}

	struct npc_scarlet_cannoneer_AI : public ScriptedAI
	{
		npc_scarlet_cannoneer_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_bomb;

		void Reset()
		{
			me->setFaction(14);
			timer_bomb = 5 * IN_MILLISECONDS;
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_bomb <= diff)
			{
				DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_BOMB);
				timer_bomb = 5 * IN_MILLISECONDS;
			}
			else
				timer_bomb -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_Armsmaster_Harlan()
{
	new Armsmaster_Harlan();
	new ScarletDefender();
	new npc_scarlet_scourge_hewer();
	new npc_scarlet_myrmidon();
	new npc_scarlet_evangelist();
	new npc_scarlet_cannoneer();
}