#include "ScriptPCH.h"

/*Comments area
The spell Greater Dragon Breath confused me.. It is like the "wash away" spell of Wise Mari from
Temple of the Jade Serpent, you can use that script changing only the spell and the knocking effect
So i won't include that spell in the script..
Same thing for spell Book Burner, it uses in-game object, the shelf.
Book Burner is used a few times during the encounter and sets a book shelf aflame, creating a
large damaging fiery zone around the shelf. Players can intercept the missile that Keogler
throws to light the book shelf by standing in its path.
At the moment the boss can cast Pyroblast, Fireball Volley and his buff "quickened mind"
The spell Teleport, teleports him in the middle of the room
*/

enum spell
{
	SPELL_BOOK_BURNER = 113364,
	SPELL_FIREBALL_VOLLEY = 113691, //interruptible
	SPELL_PYROBLAST = 113690, //interruptible
	SPELL_TELEPORT = 113626,
	SPELL_QUICKENED_MIND = 113682,
	SPELL_GREATER_DRAGON_BREATH_AURA = 113653, //Inflicts 250,000 Fire damage and an additional 50,000 Fire damage every 1 sec for 5 sec and confuses the target.
	SPELL_GREATER_DRAGON_BREATH_CHANNELED = 113641, //channeled 10 sec cast
};

class Flameweaver_Koegler : public CreatureScript
{
public:
	Flameweaver_Koegler() : CreatureScript("boss_Flameweaver_Koegler"){}

	CreatureAI* GetAI(Creature* pCreature) const
	{
		return new Flameweaver_KoeglerAI(pCreature);
	}

	struct Flameweaver_KoeglerAI : public ScriptedAI
	{
		Flameweaver_KoeglerAI(Creature *c) : ScriptedAI(c) {}

		std::list<Creature*> creatureList;
		std::list<Creature*> creatureList2;
		std::list<Player*> playerList;
		uint32 BookBurner_Timer;
		uint32 FireballVolley_Timer;
		uint32 Pyroblast_Timer;
		uint32 Teleport_Timer;
		uint32 QuickenedMind_Timer;
		uint32 DragonsBreath_timer;
		uint32 DragonsBreathDamage_timer;
		uint32 WaitTimer;
		uint32 wait;
		bool DBcasted;
		bool isCasting;
		bool teleported;

		uint32 rotTimer;
		uint32 casted;
		uint32 next_dragonsbreath;
		uint32 contaAura;

		void Reset()
		{
			contaAura = 0;
			casted = 0;
			me->RemoveAllAuras();
			QuickenedMind_Timer = 0;
			rotTimer = 0;
			Pyroblast_Timer = 1 * IN_MILLISECONDS;
			FireballVolley_Timer = 10 * IN_MILLISECONDS;
			BookBurner_Timer = 8 * IN_MILLISECONDS;
			DragonsBreath_timer = 12 * IN_MILLISECONDS;
		}

		void KilledUnit(Unit * /*victim*/)
		{
		}

		void JustDied(Unit * /*victim*/)
		{
		}

		void EnterCombat(Unit * /*who*/)
		{
			GetPlayerListInGrid(playerList, me, 1000.0f);
			for (Player* p : playerList)
			{
				p->GetMotionMaster()->MoveJump(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 100, 5.0f);
			}
			playerList.clear();
			me->SummonCreature(900001, 1272.686279, 549.542175, 12.805093, me->GetOrientation());
			GetCreatureListWithEntryInGrid(creatureList, me, 900000, 1000.0f);
			for (Creature* c : creatureList)
			{
				c->setFaction(14);
				c->SetInCombatWithZone();
				c->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
				c->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
			}
			creatureList.clear();
		}

		void MovementInform(uint32 type, uint32 /*id*/)
		{
			if (type == ROTATE_MOTION_TYPE)
				me->SetReactState(REACT_AGGRESSIVE);
		}

		bool hasQuickenedMind()
		{
			if (me->HasAura(SPELL_QUICKENED_MIND))
				return false;
			else
				return true;
		}

		void UpdateAI(const uint32 uiDiff)
		{
			if (!me->getVictim()) //out of combat
			{
			}

			if (!UpdateVictim())
				return;

			if (contaAura == 8)
			{
				contaAura = 0;
				GetCreatureListWithEntryInGrid(creatureList2, me, 90000, 1000.0f);
				for (Creature* c : creatureList2)
				{
					c->RemoveAura(73413);
				}
				creatureList2.clear();
			}

			if (me->HasUnitState(UNIT_STATE_CASTING))
			{
				if (me->HasAura(SPELL_GREATER_DRAGON_BREATH_CHANNELED))
				{
					if (rotTimer <= uiDiff)
					{
						float ori = me->GetOrientation() + M_PI / 60; // M_PI is the total value for 360� just devide it through the total ammount of rotations u wanna have (60 in the case that you wanna let him rotate over 60 seconds.
						me->SetFacingTo(ori);
						rotTimer = 1000;
					}
					else
						rotTimer -= uiDiff;
				}
				return;
			}
			//in combat
			//-----------------------------------------------------------------------------
			if (QuickenedMind_Timer <= uiDiff)
			{
				DoCast(me, SPELL_QUICKENED_MIND);
				/*if (hasQuickenedMind())
				QuickenedMind_Timer = 15 * IN_MILLISECONDS;*/
			}
			else
				QuickenedMind_Timer -= uiDiff;
			//-----------------------------------------------------------------------------
			if (Pyroblast_Timer <= uiDiff)
			{
				DoCast(me->getVictim(), SPELL_PYROBLAST);
				Pyroblast_Timer = 10 * IN_MILLISECONDS;
			}
			else Pyroblast_Timer -= uiDiff;
			//-----------------------------------------------------------------------------
			if (FireballVolley_Timer <= uiDiff)
			{
				me->CastSpell(me->getVictim(), SPELL_FIREBALL_VOLLEY, true);
				FireballVolley_Timer = 12 * IN_MILLISECONDS;
			}
			else FireballVolley_Timer -= uiDiff;
			//-----------------------------------------------------------------------------
			if (BookBurner_Timer <= uiDiff)
			{
				GetCreatureListWithEntryInGrid(creatureList, me, 90000, 1000.0f);
				for (Creature* c : creatureList)
				{
					if (!c->HasAura(113616) && !c->HasAura(73413))
					{
						contaAura++;
						DoCast(c, SPELL_BOOK_BURNER);
						c->AddAura(113616, c);
						c->AddAura(73413, c);
						BookBurner_Timer = 6 * IN_MILLISECONDS;
						creatureList.clear();
						return;
					}
				}
				creatureList.clear();
			}
			else
				BookBurner_Timer -= uiDiff;
			//-----------------------------------------------------------------------------
			if (DragonsBreath_timer <= uiDiff && casted == 0)
			{
				DoCast(me, SPELL_TELEPORT);
				DoTeleportTo(1298.130, 549.582, 12.794);
				me->SetReactState(REACT_PASSIVE);
				me->GetMotionMaster()->MoveRotate(10000, urand(0, 1) ? ROTATE_DIRECTION_LEFT : ROTATE_DIRECTION_RIGHT);
				rotTimer = 1000;
				DoCast(SPELL_GREATER_DRAGON_BREATH_CHANNELED);
				casted = 1;
				DragonsBreath_timer = 50 * IN_MILLISECONDS;
			}
			else DragonsBreath_timer -= uiDiff;

			if (next_dragonsbreath <= uiDiff)
			{
				casted = 0;
			}
			else
				next_dragonsbreath -= uiDiff;


			//-----------------------------------------------------------------------------
			DoMeleeAttackIfReady();

		}
	};
};

class npc_koegler_muro : public CreatureScript
{
public:
	npc_koegler_muro() : CreatureScript("npc_koegler_muro") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_koegler_muro_AI(creature);
	}

	struct npc_koegler_muro_AI : public ScriptedAI
	{
		npc_koegler_muro_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 s;

		void Reset()
		{
			me->setFaction(35);
			s = 0;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			switch (me->GetEntry())
			{
			case 900001://questo � quello vicino al boss
				GetPlayerListInGrid(playerList, me, 1.1f);
				for (Player* p : playerList)
				{
					if (s == 0)
					{
						me->MonsterTextEmote("non si scappa codardi.", 0);
						s++;
					}
					p->GetMotionMaster()->MoveJump(1298.130, 549.582, 12.794, 6.260676, 100, 5.0f);
				}
				playerList.clear();
				s = 0;
				break;
			}
		}
	};
};


void AddSC_Flameweaver_Koegler()
{
	new Flameweaver_Koegler();
	new npc_koegler_muro();
}