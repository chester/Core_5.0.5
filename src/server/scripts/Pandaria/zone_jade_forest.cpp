/*
* Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* ScriptData
SDName: Orgrimmar
SD%Complete: 0
SDComment: Quest support:
SDCategory: Jade Forest
EndScriptData */

/* ContentData
EndContentData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Player.h"

class npc_istruttrice_myang : public CreatureScript
{
public:
	npc_istruttrice_myang() :CreatureScript("npc_istruttrice_myang") {}

	struct npc_istruttrice_myangAI : public ScriptedAI
	{
		npc_istruttrice_myangAI(Creature* creature) : ScriptedAI(creature) { } //chiamato solo una volta

		void Reset()
		{

		}

		void UpdateAI(uint32 uiDiff)
		{
						
		}
	};
	bool OnQuestAccept(Player* player, Creature* creature, Quest const* quest)
	{
		switch (quest->GetQuestId())
		{
		case 29637:
			for (int i = 0; i < 10; i++)
			{
				creature->SummonCreature(55139, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), 0.0f);
			}
			break;
		}
		creature->AI()->SetGUID(player->GetGUID());

		return false;
	}
	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_istruttrice_myangAI(creature);
	}
};

class npc_rumpus_combatant : public CreatureScript
{
public:
	npc_rumpus_combatant() :CreatureScript("npc_rumpus_combatant") {}

	struct npc_rumpus_combatantAI : public ScriptedAI
	{
		npc_rumpus_combatantAI(Creature* creature) : ScriptedAI(creature) { } //chiamato solo una volta

		void Reset()
		{
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_UNK_15);
		}

		void UpdateAI(uint32 uiDiff)
		{

			DoMeleeAttackIfReady();
		}

		void JustDied(Unit* killer)//bug: basta ucciderne uno e la quest dovrebbe completarsi, la quest non � proprio blizzlike.
		{
			me->MonsterSay("Buon lavoro.", LANG_UNIVERSAL, 0);
			if (killer->GetTypeId() == TYPEID_PLAYER)
				killer->GetCharmerOrOwnerPlayerOrPlayerItself()->GroupEventHappens(29637, killer);
		}
	};
	
	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_rumpus_combatantAI(creature);
	}
};

void AddSC_zone_jade_forest()
{
	new npc_istruttrice_myang();
	new npc_rumpus_combatant();
}
