/*
Dungeon : StormStout Brewery / Birrificio Triplo Malto
Wise mari first boss
Jade servers
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "stormstout_brewery.h"
#include "Vehicle.h"

enum BossEventsMisc
{
	SPELL_GOING_BANANAS = 106651,
	SPELL_GROUND_POUND = 106807,
	NPC_BARREL = 56682,

	//-----sleepy hozen & drunken
	SPELL_UPPERCUT = 80182,
	//-----sodden hozen
	SPELL_WATER_STRIKE = 107046,
	//----hozen bouncer
	SPELL_DOORGUARD = 107019,

};

uint32 counter_kill = 0;

struct Location
{
	float x;
	float y;
	float z;
	float o;
};

static Location BarrelSpawns[] =
{
	{ -740.546f, 1367.330f, 146.800f, 4.413f },
	{ -763.882f, 1334.843f, 146.728f, 1.077f },
	{ -774.813f, 1364.516f, 146.731f, 6.074f },
};

class boss_ook_ook : public CreatureScript
{
public:
	boss_ook_ook() : CreatureScript("boss_ook_ook") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_ook_ook_AI(creature);
	}

	struct boss_ook_ook_AI : public ScriptedAI
	{
		boss_ook_ook_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		uint32 ground_pound_timer;
		uint32 barrel_number;
		uint32 count_going_bananas;
		std::list<Player*> listaPlayer;
		std::list<Creature*> lista;

		bool jumped;

		void Reset()
		{
			me->RemoveAllAuras();
			ground_pound_timer = 6 * IN_MILLISECONDS;
			barrel_number = 1;
			count_going_bananas = 0;
			jumped = false;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
			{
				if (counter_kill >= 40 && !jumped)
				{
					me->GetMotionMaster()->MoveJump(-756.032959, 1353.709717, 147.088242, 10, 10);
					jumped = true;
					if (me->FindNearestCreature(56862, 1000.0f, true) || me->FindNearestCreature(60276, 1000.0f, true) || me->FindNearestCreature(56783, 1000.0f, true) || me->FindNearestCreature(56927, 1000.0f, true) || me->FindNearestCreature(57097, 1000.0f, true) || me->FindNearestCreature(59684, 1000.0f, true) || me->FindNearestCreature(56924, 1000.0f, true) || me->FindNearestCreature(56863, 1000.0f, true) || me->FindNearestCreature(59605, 1000.0f, true))
					{
						GetCreatureListWithEntryInGrid(lista, me, 56862, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 60276, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 56783, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 56927, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 57097, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 59684, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 56924, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 56863, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
						GetCreatureListWithEntryInGrid(lista, me, 59605, 1000.0f);
						for (Creature * c : lista){
							c->DespawnOrUnsummon();
						}
						lista.clear();
					}
				}
			}

			if ((me->GetHealthPct() <= 90 && count_going_bananas == 0) || (me->GetHealthPct() <= 60 && count_going_bananas == 1) || (me->GetHealthPct() <= 30 && count_going_bananas == 2))
			{
				//DoCast(me, SPELL_GOING_BANANAS);
				me->CastSpell(me, SPELL_GOING_BANANAS);
				if (barrel_number == 1)
					me->SummonCreature(NPC_BARREL, -740.546f, 1367.330f, 146.800f, 4.413f);
				if (barrel_number == 2)
				{
					me->SummonCreature(NPC_BARREL, -740.546f, 1367.330f, 146.800f, 4.413f);
					me->SummonCreature(NPC_BARREL, -763.882f, 1334.843f, 146.728f, 1.077f);
				}
				if (barrel_number == 3)
				{
					me->SummonCreature(NPC_BARREL, -740.546f, 1367.330f, 146.800f, 4.413f);
					me->SummonCreature(NPC_BARREL, -763.882f, 1334.843f, 146.728f, 1.077f);
					me->SummonCreature(NPC_BARREL, -774.813f, 1364.516f, 146.731f, 6.074f);
				}
				barrel_number++;
				count_going_bananas++;
			}

			if (ground_pound_timer <= diff)
			{
				DoCast(SelectTarget(SELECT_TARGET_TOPAGGRO, 0), SPELL_GROUND_POUND);
				ground_pound_timer = 7 * IN_MILLISECONDS;
			}
			else
				ground_pound_timer -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

enum eSpells
{
	SPELL_BAREL_EXPLOSION = 106769,
	SPELL_FORCECAST_BARREL_DROP = 122385,
};

class npc_barrel : public CreatureScript
{
public:
	npc_barrel() : CreatureScript("npc_barrel") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_barrel_AI(creature);
	}

	struct npc_barrel_AI : public ScriptedAI
	{
		npc_barrel_AI(Creature* creature) : ScriptedAI(creature)
		{}
		std::list<Creature*> lista;
		std::list<Player*> listaP;
		uint32 aggiorna_movimento;
		void Reset()
		{
			aggiorna_movimento = 3.5 * IN_MILLISECONDS;
			GetCreatureListWithEntryInGrid(lista, me, 56637, 1000.0f);
			for (Creature* c : lista)
			{
				me->GetMotionMaster()->MovePoint(100, c->GetPositionX(), c->GetPositionY(), c->GetPositionZ());
			}
			lista.clear();
		}

		void MovementInform(uint32 type, uint32 id)
		{
			if (id != 100)
				return;

			float x = 0, y = 0;
			GetPositionWithDistInOrientation(me, 5.0f, me->GetOrientation(), x, y);

			me->GetMotionMaster()->MovePoint(100, x, y, me->GetPositionZ());
		}

		bool CheckIfAgainstWall()
		{
			float x = 0, y = 0;
			GetPositionWithDistInOrientation(me, 5.0f, me->GetOrientation(), x, y);

			if (!me->IsWithinLOS(x, y, me->GetPositionZ()))
				return true;

			return false;
		}

		bool CheckIfAgainstUnit()
		{
			/*if (me->SelectNearbyTarget(NULL, 1.0f))
			return true;

			return false;*/

			//check if against player
			GetPlayerListInGrid(listaP, me, 1.0f);
			for (Player* p : listaP)
			{
				if (p)
				{
					//me->MonsterSay("ho colpito un player", LANG_UNIVERSAL, 0);
					listaP.clear();
					return true;
				}
				break;
			}
			listaP.clear();
			//check if against boss
			GetCreatureListWithEntryInGrid(lista, me, 56637, 1.0f);
			for (Creature* c : lista)
			{
				if (c)
				{
					//me->MonsterSay("ho colpito il boss", LANG_UNIVERSAL, 0);
					lista.clear();
					return true;
				}
				break;
			}
			lista.clear();

			return false;
		}

		void DoExplode()
		{
			if (Vehicle* barrel = me->GetVehicleKit())
				barrel->RemoveAllPassengers();

			me->Kill(me);
			me->CastSpell(me, SPELL_BAREL_EXPLOSION, true);
		}

		void UpdateAI(const uint32 diff)
		{
			if (aggiorna_movimento <= diff)
			{
				GetCreatureListWithEntryInGrid(lista, me, 56637, 1000.0f);
				for (Creature* c : lista)
				{
					me->GetMotionMaster()->MovePoint(100, c->GetPositionX(), c->GetPositionY(), c->GetPositionZ());
				}
				lista.clear();
				aggiorna_movimento = 3.5 * IN_MILLISECONDS;
			}
			else
				aggiorna_movimento -= diff;
			if (/*CheckIfAgainstWall() || */CheckIfAgainstUnit())
				DoExplode();
		}
	};
};

/*class spell_ook_ook_barrel_ride : public SpellScriptLoader
{
public:
spell_ook_ook_barrel_ride() : SpellScriptLoader("spell_ook_ook_barrel_ride") { }

class spell_ook_ook_barrel_ride_AuraScript : public AuraScript
{
PrepareAuraScript(spell_ook_ook_barrel_ride_AuraScript);*/

//void OnApply(AuraEffect* const /*aurEff*/, AuraEffectHandleModes /*mode*/)
/*{
if (GetTarget())
if (Unit* barrelBase = GetTarget())
barrelBase->GetMotionMaster()->MoveIdle();
}

void Register()
{
OnEffectApply += AuraEffectApplyFn(spell_ook_ook_barrel_ride_AuraScript::OnApply, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
}
};

AuraScript* GetAuraScript() const
{
return new spell_ook_ook_barrel_ride_AuraScript();
}
};

class spell_ook_ook_barrel : public SpellScriptLoader
{
public:
spell_ook_ook_barrel() : SpellScriptLoader("spell_ook_ook_barrel") { }

class spell_ook_ook_barrel_AuraScript : public AuraScript
{
PrepareAuraScript(spell_ook_ook_barrel_AuraScript);

bool CheckIfAgainstWall(Unit* caster)
{
float x = caster->GetPositionX() + (2 * cos(caster->GetOrientation()));
float y = caster->GetPositionY() + (2 * sin(caster->GetOrientation()));

if (!caster->IsWithinLOS(x, y, caster->GetPositionZ()))
return true;

return false;
}

bool CheckIfAgainstUnit(Unit* caster)
{
if (caster->SelectNearbyTarget(NULL, 1.0f))
return true;

return false;
}

void OnUpdate(uint32 diff)
{
Unit* caster = GetCaster();
if (!caster)
return;

if (CheckIfAgainstWall(caster) || CheckIfAgainstUnit(caster))
{
if (Vehicle* barrel = caster->GetVehicle())
{
barrel->RemoveAllPassengers();

if (Unit* barrelBase = barrel->GetBase())
{
barrelBase->CastSpell(barrelBase, SPELL_BAREL_EXPLOSION, true);
barrelBase->Kill(barrelBase);
}
}

caster->CastSpell(caster, SPELL_FORCECAST_BARREL_DROP, true);
caster->RemoveAurasDueToSpell(GetSpellInfo()->Id);
}
}

void Register()
{
OnAuraUpdate += AuraUpdateFn(spell_ook_ook_barrel_AuraScript::OnUpdate);
}
};

AuraScript* GetAuraScript() const
{
return new spell_ook_ook_barrel_AuraScript();
}
};*/

class npc_drunken_hozen_brawler : public CreatureScript
{
public:
	npc_drunken_hozen_brawler() : CreatureScript("npc_drunken_hozen_brawler") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_drunken_hozen_brawler_AI(creature);
	}

	struct npc_drunken_hozen_brawler_AI : public ScriptedAI
	{
		npc_drunken_hozen_brawler_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		uint32 uppercut_timer;

		void Reset()
		{
			me->setFaction(14);
			uppercut_timer = 2000;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			counter_kill++;
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (uppercut_timer <= diff)
			{
				DoCast(me->getVictim(), SPELL_UPPERCUT);
				uppercut_timer = 8000;
			}
			else
				uppercut_timer -= diff;
		}
	};
};

class npc_hozen_general : public CreatureScript
{
public:
	npc_hozen_general() : CreatureScript("npc_hozen_general") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_hozen_general_AI(creature);
	}

	struct npc_hozen_general_AI : public ScriptedAI
	{
		npc_hozen_general_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers


		void Reset()
		{
			me->setFaction(7);
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			counter_kill++;
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{

		}
	};
};

class npc_sodden_hozen_brawler : public CreatureScript
{
public:
	npc_sodden_hozen_brawler() : CreatureScript("npc_sodden_hozen_brawler") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_sodden_hozen_brawler_AI(creature);
	}

	struct npc_sodden_hozen_brawler_AI : public ScriptedAI
	{
		npc_sodden_hozen_brawler_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		uint32 water_strike_timer;

		void Reset()
		{
			me->setFaction(14);
			water_strike_timer = 5000;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			counter_kill++;
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (water_strike_timer <= diff)
			{
				DoCast(me->getVictim(), SPELL_WATER_STRIKE);
				water_strike_timer = 8000;
			}
			else
				water_strike_timer -= diff;
		}
	};
};

class npc_sleepy_hozen_brawler : public CreatureScript
{
public:
	npc_sleepy_hozen_brawler() : CreatureScript("npc_sleepy_hozen_brawler") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_sleepy_hozen_brawler_AI(creature);
	}

	struct npc_sleepy_hozen_brawler_AI : public ScriptedAI
	{
		npc_sleepy_hozen_brawler_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		uint32 uppercut_timer;

		void Reset()
		{
			me->setFaction(7);
			uppercut_timer = 2000;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			counter_kill++;
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (uppercut_timer <= diff)
			{
				DoCast(me->getVictim(), SPELL_UPPERCUT);
				uppercut_timer = 8000;
			}
			else
				uppercut_timer -= diff;
		}
	};
};

class npc_hozen_bouncer : public CreatureScript
{
public:
	npc_hozen_bouncer() : CreatureScript("npc_hozen_bouncer") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_hozen_bouncer_AI(creature);
	}

	struct npc_hozen_bouncer_AI : public ScriptedAI
	{
		npc_hozen_bouncer_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		uint32 doorguard_timer;
		std::list<Player*> listaPlayer;
		std::list<Creature*> lista;

		void Reset()
		{
			me->setFaction(14);
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
			DoCast(me, SPELL_DOORGUARD);
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			counter_kill++;
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			GetPlayerListInGrid(listaPlayer, me, 10.0f);
			for (Player* player : listaPlayer)
			{
				player->NearTeleportTo(-756.032959, 1353.709717, 147.088242, 1.718477);
			}
			listaPlayer.clear();
			GetCreatureListWithEntryInGrid(lista, me, 56637, 1000.0f);
			for (Creature* c : lista)
			{
				if (c->isDead())
				{
					me->DespawnOrUnsummon();
				}
			}
		}
	};
};

void AddSC_boss_ook_ook()
{
	new boss_ook_ook();
	new npc_barrel();
	//new spell_ook_ook_barrel_ride();
	//new spell_ook_ook_barrel();
	new npc_drunken_hozen_brawler();
	new npc_sleepy_hozen_brawler();
	new npc_sodden_hozen_brawler();
	new npc_hozen_general();
	new npc_hozen_bouncer();
}
