#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "stormstout_brewery.h"
#include "Vehicle.h"

enum BossEventsMisc
{
	SPELL_CARROT_BREATH = 112944,
	SPELL_FURLWIND = 112992,
	NPC_HOPLING = 60208,
	NPC_HOPPER = 59464,
	SPELL_HOPPER_EXPLOSIVE_BREW = 114291,
	NPC_BOPPER = 59551,
};

class boss_hoptallus : public CreatureScript
{
public:
	boss_hoptallus() : CreatureScript("boss_hoptallus") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_hoptallus_AI(creature);
	}

	struct boss_hoptallus_AI : public ScriptedAI
	{
		boss_hoptallus_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		bool isCasting;
		bool VomitoCasted;
		bool Casted;
		bool moving;
		bool Bopper_spawned;
		bool Hopper_spawned;
		bool Hopling_spawned;
		bool jumped;
		uint32 WaitTimer;
		uint32 VomitoTimer;
		uint32 furlwind_timer;
		uint32 start_summon_minion_timer;
		uint32 hopper_spawn_timer;
		uint32 bopper_spawn_timer;
		uint32 carrot_channel;
		std::list<Creature*> lista;
		std::list<Player*> listaPlayer;

		void Reset()
		{
			jumped = false;
			WaitTimer = 500;
			VomitoTimer = 10 * IN_MILLISECONDS;
			VomitoCasted = false;
			furlwind_timer = 6 * IN_MILLISECONDS;
			moving = false;
			start_summon_minion_timer = 20 * IN_MILLISECONDS;
			Hopling_spawned = false;
			Hopper_spawned = true;
			Bopper_spawned = true;
			carrot_channel = 15 * IN_MILLISECONDS;
			if (me->FindNearestCreature(NPC_HOPLING, 60.0f, true) || me->FindNearestCreature(NPC_HOPPER, 60.0f, true) || me->FindNearestCreature(NPC_BOPPER, 60.0f, true))
			{
				GetCreatureListWithEntryInGrid(lista, me, NPC_HOPLING, 60.0f);
				for (Creature * c : lista){
					c->DespawnOrUnsummon();
				}
				lista.clear();
				GetCreatureListWithEntryInGrid(lista, me, NPC_HOPPER, 60.0f);
				for (Creature * c : lista){
					c->DespawnOrUnsummon();
				}
				lista.clear();
				GetCreatureListWithEntryInGrid(lista, me, NPC_BOPPER, 60.0f);
				for (Creature * c : lista){
					c->DespawnOrUnsummon();
				}
				lista.clear();
			}
		}

		void EnterCombat(Unit* /*who*/)
		{
			me->GetMotionMaster()->MoveChase(SelectTarget(SELECT_TARGET_TOPAGGRO, 0));
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			if (me->FindNearestCreature(NPC_HOPLING, 60.0f, true) || me->FindNearestCreature(NPC_HOPPER, 60.0f, true) || me->FindNearestCreature(NPC_BOPPER, 60.0f, true))
			{
				GetCreatureListWithEntryInGrid(lista, me, NPC_HOPLING, 60.0f);
				for (Creature * c : lista){
					c->DespawnOrUnsummon();
				}
				lista.clear();
				GetCreatureListWithEntryInGrid(lista, me, NPC_HOPPER, 60.0f);
				for (Creature * c : lista){
					c->DespawnOrUnsummon();
				}
				lista.clear();
				GetCreatureListWithEntryInGrid(lista, me, NPC_BOPPER, 60.0f);
				for (Creature * c : lista){
					c->DespawnOrUnsummon();
				}
				lista.clear();
			}
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (me->HasUnitState(UNIT_STATE_CASTING))
				return;

			if (VomitoTimer <= diff)
			{
				VomitoTimer = 30 * IN_MILLISECONDS;
				DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_CARROT_BREATH);
			}
			else
				VomitoTimer -= diff;


			if (furlwind_timer <= diff)
			{
				furlwind_timer = 15 * IN_MILLISECONDS;
				me->CastSpell(me, SPELL_FURLWIND, true);
			}
			else
				furlwind_timer -= diff;

			DoMeleeAttackIfReady();
			//-----------------------------------------------------------------------
			if (!Hopling_spawned)
			{
				if (start_summon_minion_timer <= diff)
				{
					for (int i = 0; i < 4; i++)
					{
						switch (i)
						{
						case 0:
							me->SummonCreature(NPC_HOPLING, -708.807617, 1269.666992, 162.786346, 5.717774);

							break;
						case 1:
							me->SummonCreature(NPC_HOPLING, -700.345825, 1247.204102, 162.794250, 1.409864);

							break;
						case 2:
							me->SummonCreature(NPC_HOPLING, -689.138916, 1256.982422, 162.794754, 2.576180);

							break;
						case 3:
							me->SummonCreature(NPC_HOPLING, -693.721497, 1273.831665, 162.790482, 4.039379);
							Hopling_spawned = true;
							Hopper_spawned = false;
							hopper_spawn_timer = 5 * IN_MILLISECONDS;
							break;
						}
					}
				}
				else
					start_summon_minion_timer -= diff;
			}

			if (!Hopper_spawned)
			{
				if (hopper_spawn_timer <= diff)
				{
					for (int j = 0; j < 4; j++)
					{
						switch (j)
						{
						case 0:
							me->SummonCreature(NPC_HOPPER, -708.807617, 1269.666992, 162.786346, 5.717774);

							break;
						case 1:
							me->SummonCreature(NPC_HOPPER, -700.345825, 1247.204102, 162.794250, 1.409864);

							break;
						case 2:
							me->SummonCreature(NPC_HOPPER, -689.138916, 1256.982422, 162.794754, 2.576180);

							break;
						case 3:
							me->SummonCreature(NPC_HOPPER, -693.721497, 1273.831665, 162.790482, 4.039379);
							Bopper_spawned = false;
							Hopper_spawned = true;
							bopper_spawn_timer = 5 * IN_MILLISECONDS;
							break;
						}
					}
				}
				else
					hopper_spawn_timer -= diff;
			}

			if (!Bopper_spawned)
			{
				if (bopper_spawn_timer <= diff)
				{
					for (int k = 0; k < 4; k++)
					{
						switch (k)
						{
						case 0:
							me->SummonCreature(NPC_BOPPER, -708.807617, 1269.666992, 162.786346, 5.717774);

							break;
						case 1:
							me->SummonCreature(NPC_BOPPER, -700.345825, 1247.204102, 162.794250, 1.409864);

							break;
						case 2:
							me->SummonCreature(NPC_BOPPER, -689.138916, 1256.982422, 162.794754, 2.576180);

							break;
						case 3:
							me->SummonCreature(NPC_BOPPER, -693.721497, 1273.831665, 162.790482, 4.039379);
							Hopling_spawned = false;
							Bopper_spawned = true;
							start_summon_minion_timer = 20 * IN_MILLISECONDS;
							break;
						}
					}
				}
				else
					bopper_spawn_timer -= diff;
			}
		}

	};
};

class npc_hopper : public CreatureScript
{
public:
	npc_hopper() : CreatureScript("npc_hopper") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_hopper_AI(creature);
	}

	struct npc_hopper_AI : public ScriptedAI
	{
		npc_hopper_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Player*> playerList;

		void Reset()
		{

		}

		void EnterCombat(Unit* /*who*/)
		{
			me->setFaction(14);
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			GetPlayerListInGrid(playerList, me, 60.0f);
			for (Player* player : playerList)
			{
				me->GetMotionMaster()->MovePoint(1, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ());
				if ((uint32)me->GetPositionX() == (uint32)player->GetPositionX() && (uint32)me->GetPositionY() == (uint32)player->GetPositionY() && (uint32)me->GetPositionZ() == (uint32)player->GetPositionZ())
				{
					DoCast(me, SPELL_HOPPER_EXPLOSIVE_BREW);
					me->Kill(me);
				}
				playerList.clear();
			}
		}
	};
};

class npc_bopper : public CreatureScript
{
public:
	npc_bopper() : CreatureScript("npc_bopper") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_bopper_AI(creature);
	}

	struct npc_bopper_AI : public ScriptedAI
	{
		npc_bopper_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers


		void Reset()
		{

		}

		void EnterCombat(Unit* /*who*/)
		{
			me->setFaction(14);
		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{

		}
	};
};

void AddSC_Hoptallus()
{
	new boss_hoptallus();
	new npc_bopper();
	new npc_hopper();
}