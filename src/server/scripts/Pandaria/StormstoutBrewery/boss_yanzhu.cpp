#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "stormstout_brewery.h"
#include "Vehicle.h"

class boss_yanzhu : public CreatureScript
{
public:
	boss_yanzhu() : CreatureScript("boss_yanzhu") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_yanzhu_AI(creature);
	}

	struct boss_yanzhu_AI : public BossAI
	{
		boss_yanzhu_AI(Creature* creature) : BossAI(creature, DATA_OOK_OOK)
		{}

		//timers


		void Reset()
		{

		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{
			_JustDied();
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{

		}
	};
};