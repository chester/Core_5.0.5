#include "ScriptPCH.h"
#include "ScriptedEscortAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Unit.h"
#include "ScriptedFollowerAI.h"
#include "FollowerReference.h"

enum boss_spell
{
	SPELL_BLAZING_FISTS = 114808,
	SPELL_FIRESTORM_KICK = 113764,
	SPELL_RISING_FLAME = 114410,
	SPELL_SCORCHED_EARTH = 114460,
};

enum boss_text
{
	text_on_aggro = 0,
	text_blazing_fists = 3,
	text_reset = 2,
	text_on_death = 1,
	text_on_kill = 4,

};

class boss_korloff : public CreatureScript
{
public:
	boss_korloff() : CreatureScript("boss_korloff") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_korloff_AI(creature);
	}

	struct boss_korloff_AI : public ScriptedAI
	{
		boss_korloff_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Creature*> creatureList2;
		std::list<Player*> playerList;
		uint32 timer_blazing_fists;
		uint32 timer_firestorm_kick;
		uint32 timer_wait;
		uint32 n;
		uint32 v;
		uint32 count_talk;
		bool gira;

		void Reset()
		{
			me->setFaction(14);
			timer_blazing_fists = 3 * IN_MILLISECONDS;
			timer_firestorm_kick = 7 * IN_MILLISECONDS;
			Talk(text_reset);
			gira = false;
			n = 0;
			v = 0;
			count_talk = 0;
			timer_wait = 7000;
		}

		void EnterCombat(Unit* /*who*/)
		{
			Talk(text_on_aggro);
		}

		void KilledUnit(Unit* /*victim*/)
		{
			Talk(text_on_kill);
		}

		void JustDied(Unit* /*killer*/)
		{
			Talk(text_on_death);
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (me->HasUnitState(UNIT_STATE_CASTING))
			{
				timer_firestorm_kick = 10000;
				return;
			}


				if (timer_blazing_fists <= diff)
				{
					if (count_talk%2 != 0)//shut the fuck up idiot
						Talk(text_blazing_fists);
					DoCast(me->getVictim(), SPELL_BLAZING_FISTS);
					timer_blazing_fists = 5 * IN_MILLISECONDS;
					count_talk++;
				}
				else
					timer_blazing_fists -= diff;


				if (timer_firestorm_kick <= diff)
				{
					Unit* p = SelectTarget(SELECT_TARGET_RANDOM, 0);
					float x = p->GetPositionX();
					float y = p->GetPositionY();
					float z = p->GetPositionZ();
					me->GetMotionMaster()->MoveJump(x, y, z+1, 100, 100, 0);
					DoCast(SPELL_FIRESTORM_KICK);
				}
				else
					timer_firestorm_kick -= diff;

			if ((me->GetHealthPct() <= 90 && v == 0) || (me->GetHealthPct() <= 80 && v == 1) || (me->GetHealthPct() <= 70 && v == 2) || (me->GetHealthPct() <= 60 && v == 3) || (me->GetHealthPct() <= 50 && v == 4) || (me->GetHealthPct() <= 40 && v == 5) || (me->GetHealthPct() <= 30 && v == 6) || (me->GetHealthPct() <= 20 && v == 7) || (me->GetHealthPct() <= 10 && v == 8))
			{
				DoCast(me, SPELL_RISING_FLAME);
				v++;
			}

			if (me->GetHealthPct() <= 50 && n == 0)
			{
				n++;
				DoCast(me, SPELL_SCORCHED_EARTH);
			}

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_korloff()
{
	new boss_korloff();
}