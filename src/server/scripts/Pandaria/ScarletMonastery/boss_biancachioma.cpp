#include "ScriptPCH.h"
#include "ScriptedEscortAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Unit.h"
#include "ScriptedFollowerAI.h"
#include "FollowerReference.h"

enum boss_durand_spell
{
	SPELL_FLASH_OF_STEEL = 115629,
	SPELL_DASHING_STRIKE = 115739,
};

enum Spells
{
	//Mograine Spells
	SPELL_CRUSADERSTRIKE = 14518,
	SPELL_HAMMEROFJUSTICE = 5589,
	SPELL_LAYONHANDS = 9257,
	SPELL_RETRIBUTIONAURA = 8990,

	//Whitemanes Spells
	SPELL_DEEPSLEEP = 9256,
	SPELL_SCARLETRESURRECTION = 9232,
	SPELL_MASSRESURRECTION_H = 113134,
	SPELL_DOMINATEMIND = 14515,
	SPELL_HOLYSMITE = 9481,
	SPELL_HOLYSMITE_H = 114848,
	SPELL_HEAL = 12039,
	SPELL_POWERWORDSHIELD = 22187,
	SPELL_POWERWORDSHIELD_H = 127399,
};

enum boss_text
{
	text_on_aggro = 0,
	text_see_player = 1,
	text_random = 3,
	text_on_death = 4,
	text_on_kill = 2,
	//Whitemane says
	SAY_WH_INTRO = 3,
	SAY_WH_KILL = 4,
	SAY_WH_RESSURECT = 5,
};

bool second_time = false;
bool isSecondSummoned = false;

class boss_commander_durand : public CreatureScript
{
public:
	boss_commander_durand() : CreatureScript("boss_commander_durand") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_commander_durand_AI(creature);
	}

	struct boss_commander_durand_AI : public ScriptedAI
	{
		boss_commander_durand_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Creature*> creatureList2;
		std::list<Player*> playerList;
		uint32 timer_flash_of_steel;
		uint32 timer_dashing_strike;
		uint32 count_flash;
		uint64 guid1;
		uint64 guid2;
		uint64 guid3;
		uint64 guid4;
		uint32 count_dashing_strike;

		void Reset()
		{
			me->setFaction(14);
			timer_flash_of_steel = 3 * IN_MILLISECONDS;
			timer_dashing_strike = 7 * IN_MILLISECONDS;
			count_flash = 0;
			guid2 = 0;
			guid1 = 0;
			guid3 = 0;
			guid4 = 0;
		}

		void EnterCombat(Unit* /*who*/)
		{
			Talk(text_on_aggro);
		}

		void KilledUnit(Unit* /*victim*/)
		{
			Talk(text_on_kill);
		}

		void JustDied(Unit* /*killer*/)
		{
			Talk(text_on_death);
			if (!isSecondSummoned)//questa � una variabile booleana (vero, falso) globale. Se il boss non � stato summonato per la seconda volta...
			{
				isSecondSummoned = true;//metto a true per non entrare ancora qua dentro quando questo npc verr� resuscitato da quello che si sta per summonare
				me->SummonCreature(3977, 712.269775, 605.833252, 11.509049, 0.000580);//facendo cos� summona questo npc solo 1 volta perch� dentro questo if appunto non ci entra piu.
			}
			if (!second_time)//despawna il boss se non � stato ancora chiamato l'evento per resuscitarlo
				me->DespawnOrUnsummon();
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (me->HasUnitState(UNIT_STATE_CHARGING))
			{

				return;
			}

			if (timer_flash_of_steel <= diff)//la spell era buggata, gli ho fatto un hack fix. In poche parole dovrebbe colpire 5 volte 5 player diversi.
			{
				if (count_flash == 5)//count flash parte da 0 al reset, quindi se arriva a 5 significa che ha colpito 5 player quindi posso settare un nuovo timer.
				{
					count_flash = 0;//resetto il counter per la prossima volta che user� questa spell
					timer_flash_of_steel = 9 * IN_MILLISECONDS;
				}

				Unit* p = SelectTarget(SELECT_TARGET_RANDOM, 0);
				guid3 = p->GetGUID();//prendo la guid del player random
				if (guid3 != guid4)//se la guid del player random 1 � diversa dalla seconda guid (ovvero la guid precedente)
				{
					count_flash++;
					DoCast(p, SPELL_FLASH_OF_STEEL);//casto la spell
					guid4 = guid3;//ora mi salvo questa guid3 dentro la 4 perch� nella guid3 al prossimo giro ci va una guid nuova.  Dovremo poi confrontare queste due guid grazie all'if che c'� sopra
				}
			}
			else
				timer_flash_of_steel -= diff;


			if (timer_dashing_strike <= diff)//stessa cosa di sopra.
			{
				//if (count_dashing_strike == 5)
				//{
				//count_dashing_strike = 0;
				timer_dashing_strike = 13000;
				//}
				Unit* p = SelectTarget(SELECT_TARGET_RANDOM, 0);
				//guid1 = p->GetGUID();

				//if (guid1 != guid2)
				//{
				DoCast(p, SPELL_DASHING_STRIKE);
				//count_dashing_strike++;
				//guid2 = guid1;
				//}
			}
			else
				timer_dashing_strike -= diff;


			DoMeleeAttackIfReady();
		}
	};
};

class boss_high_inquisitor_whitemane : public CreatureScript
{
public:
	boss_high_inquisitor_whitemane() : CreatureScript("boss_high_inquisitor_whitemane") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_high_inquisitor_whitemaneAI(creature);
	}

	struct boss_high_inquisitor_whitemaneAI : public ScriptedAI
	{
		boss_high_inquisitor_whitemaneAI(Creature* creature) : ScriptedAI(creature)
		{
		}

		uint32 Heal_Timer;
		uint32 PowerWordShield_Timer;
		uint32 HolySmite_Timer;
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 count_resurrect;
		uint32 mass_ress_timer;

		bool _bCanResurrectCheck;
		bool _bCanResurrect;

		bool resurrected;
		bool isResurrect;

		void Reset()
		{
			count_resurrect = 0;
			Heal_Timer = 10000;
			PowerWordShield_Timer = 15000;
			HolySmite_Timer = 6000;
			resurrected = false;
			isResurrect = false;

			me->GetMotionMaster()->MovePoint(1, 741.484985, 606.145386, 15.027293);
		}

		void EnterCombat(Unit* /*who*/)
		{
			Talk(SAY_WH_INTRO);
		}

		void KilledUnit(Unit* /*victim*/)
		{
			Talk(SAY_WH_KILL);
		}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (me->HasUnitState(UNIT_STATE_CASTING))
				return;

			if (!second_time)
				second_time = true;

			//enter here 1 time
			if (me->GetHealthPct() <= 80 && !resurrected)
			{
				GetPlayerListInGrid(playerList, me, 1000.0f);
				GetCreatureListWithEntryInGrid(creatureList, me, 60040, 1000.0f);
				for (Creature* c : creatureList)
				{
					for (Player* p : playerList)
					{
						DoCast(p, SPELL_DEEPSLEEP);
					}
					playerList.clear();
					Talk(SAY_WH_RESSURECT);
					resurrected = true;
					isResurrect = true;
					DoCast(c, SPELL_SCARLETRESURRECTION);//casta la resurrezione solo per EFFETTO GRAFICO, l'npc non pu� accettare un resurrect quindi..
					c->DespawnOrUnsummon();//dobbiamo despawnarlo
					me->SummonCreature(60040, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation()); // e risummonarlo
					break;
				}
				creatureList.clear();
			}
			/*if (isResurrect)
			{

			}*/
			//PowerWordShield_Timer
			if (PowerWordShield_Timer <= diff)
			{
				if (me->GetMap()->IsHeroic())
					DoCast(me, SPELL_POWERWORDSHIELD_H);
				else
					DoCast(me, SPELL_POWERWORDSHIELD);
				PowerWordShield_Timer = 15000;
			}
			else PowerWordShield_Timer -= diff;

			//HolySmite_Timer
			if (HolySmite_Timer <= diff)
			{
				if (me->GetMap()->IsHeroic())
					DoCastVictim(SPELL_HOLYSMITE_H);
				else
					DoCastVictim(SPELL_HOLYSMITE);
				HolySmite_Timer = 1520;
			}
			else HolySmite_Timer -= diff;

			if ((me->GetHealthPct() <= 75 && count_resurrect == 0) || (me->GetHealthPct() <= 25 && count_resurrect == 1))
			{
				DoCast(SPELL_MASSRESURRECTION_H);
				count_resurrect++;
			}

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_durand_and_whitemane()
{
	new boss_commander_durand();
	new boss_high_inquisitor_whitemane();
}