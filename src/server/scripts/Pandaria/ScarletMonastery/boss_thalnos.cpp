#include "ScriptPCH.h"
#include "ScriptedEscortAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Unit.h"
#include "ScriptedFollowerAI.h"
#include "FollowerReference.h"

enum boss_spell
{
	SPELL_RAISE_FALLEN_CRUSADER = 115139,
	SPELL_SPIRIT_GALE = 115289,
	SPELL_EVICT_SOUL = 115297,
};

enum boss_text
{
	text_on_aggro = 3,
	text_summon_crusader_cast1 = 2,
	text_summon_crusader_cast2 = 6,
	text_evict_soul = 5,
	text_on_death = 1,
	text_on_kill = 4,

};

class boss_thalnos : public CreatureScript
{
public:
	boss_thalnos() : CreatureScript("boss_thalnos") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_thalnos_AI(creature);
	}

	struct boss_thalnos_AI : public ScriptedAI
	{
		boss_thalnos_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Creature*> creatureList2;
		std::list<Player*> playerList;
		uint32 timer_raise_fallen_crusader;
		uint32 timer_spirit_gale;
		uint32 timer_evict_soul;
		uint32 random;

		void Reset()
		{
			me->setFaction(14);
			timer_raise_fallen_crusader = 9 * IN_MILLISECONDS;
			timer_spirit_gale = 5 * IN_MILLISECONDS;
			timer_evict_soul = 3 * IN_MILLISECONDS;
			GetCreatureListWithEntryInGrid(creatureList2, me, 59974, 30.0f);
			for (Creature* c : creatureList2)
			{
				c->DespawnOrUnsummon();
			}
			creatureList2.clear();

			GetCreatureListWithEntryInGrid(creatureList2, me, 59893, 30.0f);
			for (Creature* c : creatureList2)
			{
				c->DespawnOrUnsummon();
			}
			creatureList2.clear();

			GetCreatureListWithEntryInGrid(creatureList2, me, 59930, 30.0f);
			for (Creature* c : creatureList2)
			{
				c->DespawnOrUnsummon();
			}
			creatureList2.clear();
		}

		void EnterCombat(Unit* /*who*/)
		{
			Talk(text_on_aggro);
		}

		void KilledUnit(Unit* /*victim*/)
		{
			Talk(text_on_kill);
		}

		void JustDied(Unit* /*killer*/)
		{
			Talk(text_on_death);
			GetCreatureListWithEntryInGrid(creatureList2, me, 59974, 30.0f);
			for (Creature* c : creatureList2)
			{
				c->DespawnOrUnsummon();
			}
			creatureList2.clear();

			GetCreatureListWithEntryInGrid(creatureList2, me, 59893, 30.0f);
			for (Creature* c : creatureList2)
			{
				c->DespawnOrUnsummon();
			}
			creatureList2.clear();

			GetCreatureListWithEntryInGrid(creatureList2, me, 59930, 30.0f);
			for (Creature* c : creatureList2)
			{
				c->DespawnOrUnsummon();
			}
			creatureList2.clear();
		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_raise_fallen_crusader <= diff)
			{
				random = rand() % 2;
				if (random == 0)
					Talk(text_summon_crusader_cast1);
				else
					Talk(text_summon_crusader_cast2);

				DoCast(SPELL_RAISE_FALLEN_CRUSADER);
				timer_raise_fallen_crusader = 15 * IN_MILLISECONDS;
			}
			else
				timer_raise_fallen_crusader -= diff;

			if (timer_evict_soul <= diff)
			{
				Unit* p = SelectTarget(SELECT_TARGET_RANDOM, 0);
				DoCast(p, SPELL_EVICT_SOUL);
				me->SummonCreature(59893, p->GetPositionX(), p->GetPositionY(), p->GetPositionZ(), p->GetOrientation());
				me->SummonCreature(59893, p->GetPositionX(), p->GetPositionY(), p->GetPositionZ(), p->GetOrientation());
				timer_evict_soul = 7 * IN_MILLISECONDS;
			}
			else
				timer_evict_soul -= diff;

			if (timer_spirit_gale <= diff)
			{
				DoCast(me->getVictim(), SPELL_SPIRIT_GALE);
				timer_spirit_gale = 11 * IN_MILLISECONDS;
			}
			else
				timer_spirit_gale -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

enum boss_adds_spells
{
	SPELL_EVICTED_SOUL = 115309,
	SPELL_SIPHON_ESSENCE = 40921,
	SPELL_EMPOWERING_SPIRIT = 115157,
};

//boss adds id (59974, 59893, 59930)
class npc_boss_adds : public CreatureScript
{
public:
	npc_boss_adds() : CreatureScript("npc_boss_adds_first_scarletmonastery") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_boss_adds_AI(creature);
	}

	struct npc_boss_adds_AI : public ScriptedAI
	{
		npc_boss_adds_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_evicted_soul;
		uint32 timer_siphon_essence;
		uint32 timer_empowering_spirit;

		void Reset()
		{
			switch (me->GetEntry())
			{
			case 59974:
				me->setFaction(14);
				timer_siphon_essence = 5 * IN_MILLISECONDS;
				timer_evicted_soul = 1 * IN_MILLISECONDS;
				break;
			case 59893:
				me->setFaction(14);
				timer_empowering_spirit = 3 * IN_MILLISECONDS;
				break;
			case 59930:
				me->setFaction(14);
				break;
			default:
				break;
			}
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			switch (me->GetEntry())
			{
			case 59974:
				if (me->GetMap()->IsHeroic())
				{
					if (timer_siphon_essence <= diff)
					{
						DoCast(me, SPELL_SIPHON_ESSENCE);
						timer_siphon_essence = 10 * IN_MILLISECONDS;
					}
					else
						timer_siphon_essence -= diff;
				}
				if (timer_evicted_soul <= diff)
				{
					DoCast(SelectTarget(SELECT_TARGET_RANDOM, 0), SPELL_EVICTED_SOUL);
				}
				else
					timer_evicted_soul -= diff;
				DoMeleeAttackIfReady();
				break;
			case 59893:
				if (timer_empowering_spirit <= diff)
				{
					GetCreatureListWithEntryInGrid(creatureList, me, 59974, 60.0f);
					for (Creature* c : creatureList)
					{
						if (c->isDead())
						{
							DoCast(c, SPELL_EMPOWERING_SPIRIT);
							break;
						}
					}
					creatureList.clear();
					timer_empowering_spirit = 3 * IN_MILLISECONDS;
				}
				timer_empowering_spirit -= diff;
				DoMeleeAttackIfReady();
				break;
			case 59930:
				DoMeleeAttackIfReady();
				break;
			default:
				break;
			}

			
		}
	};
};

void AddSC_boss_thalnos()
{
	new boss_thalnos();
	new npc_boss_adds();
}