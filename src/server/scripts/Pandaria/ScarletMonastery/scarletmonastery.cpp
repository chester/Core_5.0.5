#include "ScriptPCH.h"
#include "ScriptedEscortAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Unit.h"
#include "ScriptedFollowerAI.h"
#include "FollowerReference.h"

//Scarlet Centurion - 59746

enum Scarlet_centurion_spells
{
	SPELL_CLEAVE = 115519,
	SPELL_RETALIATION = 115511,
};

class npc_scarlet_centurion : public CreatureScript
{
public:
	npc_scarlet_centurion() : CreatureScript("npc_scarlet_centurion") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_centurion_AI(creature);
	}

	struct npc_scarlet_centurion_AI : public ScriptedAI
	{
		npc_scarlet_centurion_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_cleave;
		uint32 timer_retaliation;

		void Reset()
		{
			me->setFaction(14);
			timer_cleave = 5 * IN_MILLISECONDS;
			timer_retaliation = 7 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* /*who*/)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*/)
		{
		}

		void JustDied(Unit* /*killer*/)
		{

		}

		void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_cleave <= diff)
			{
				DoCast(me->getVictim(), SPELL_CLEAVE);
				timer_cleave = 5 * IN_MILLISECONDS;
			}
			else
				timer_cleave -= diff;

			if (timer_retaliation <= diff)
			{
				DoCast(me, SPELL_RETALIATION);
				timer_retaliation = 7 * IN_MILLISECONDS;
			}
			else
				timer_retaliation -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

/*enum Scarlet_fanatic_spells
{
	
};*/

//Scarlet Fanatic - 58555
/*class npc_scarlet_fanatic : public CreatureScript
{
public:
	npc_scarlet_fanatic() : CreatureScript("npc_scarlet_fanatic") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_scarlet_fanatic_AI(creature);
	}

	struct npc_scarlet_fanatic_AI : public ScriptedAI
	{
		npc_scarlet_fanatic_AI(Creature* creature) : ScriptedAI(creature)
		{}

		//timers
		std::list<Creature*> creatureList;
		std::list<Player*> playerList;
		uint32 timer_cleave;
		uint32 timer_retaliation;

		void Reset()
		{
			me->setFaction(14);
			timer_cleave = 5 * IN_MILLISECONDS;
			timer_retaliation = 7 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* /*who*//*)
		{

		}

		void DoAction(const int32 action)
		{
		}

		void KilledUnit(Unit* /*victim*//*)
		{
		}

		void JustDied(Unit* /*killer*//*)
		{

		}

		void DamageTaken(Unit* /*attacker*//*, uint32& /*damage*//*)
		{

		}

		void MoveInLineOfSight(Unit* who)
		{}

		void UpdateAI(const uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (timer_cleave <= diff)
			{
				DoCast(me->GetVictim(), SPELL_CLEAVE);
				timer_cleave = 5 * IN_MILLISECONDS;
			}
			else
				timer_cleave -= diff;

			if (timer_retaliation <= diff)
			{
				DoCast(me, SPELL_RETALIATION);
				timer_retaliation = 7 * IN_MILLISECONDS;
			}
			else
				timer_retaliation -= diff;

			DoMeleeAttackIfReady();
		}
	};
};*/

void AddSC_scarletmonastery()
{
	new npc_scarlet_centurion();
	//new npc_scarlet_fanatic();
}