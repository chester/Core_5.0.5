/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: Orgrimmar
SD%Complete: 0
SDComment: Quest support:
SDCategory: Orgrimmar
EndScriptData */

/* ContentData
EndContentData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Player.h"

enum Yells
{

};

enum Spells
{
	SPELL_LAMPO_MONACO = 117952,
	SPELL_CURA_MONACO = 115072,
	SPELL_FLAME_SHOCK_SHAMANO = 8050,
	SPELL_LAVA_BURST = 51505,
	SPELL_ALI_PALADINO = 31884,
	SPELL_BOLLA_PALADINO = 642,
	SPELL_MARTELLO_PALADINO = 24275,
	SPELL_DEATH_GRIP = 49576,
	SPELL_DEATH_STRIKE = 49998,
	SPELL_CLEAVE_WARRIOR = 845,
	SPELL_BUFF_ATTACK_POWER = 6673,
	SPELL_FAN_OF_KNIVES = 51723,
	SPELL_LAVA_SOURGE = 77756
};

class mob_gamon : public CreatureScript
{
public:
	mob_gamon() :CreatureScript("mob_gamon") {}

	struct mob_gamonAI : public ScriptedAI
	{
		mob_gamonAI(Creature* creature) : ScriptedAI(creature) { } //chiamato solo una volta

		uint32 timer_buff_attack_power;
		uint32 timer_lampo_monaco;
		uint32 timer_cleave;
		uint32 timer_fan;
		uint32 timer_death_strike;
		uint32 timer_bolla;
		uint32 timer_ali;
		uint32 timer_martello;
		uint32 timer_death_grip;
		uint32 timer_lava_burst;
		uint32 timer_flame_shock;
		uint32 timer_cura_monaco;
		uint32 timer_lava_surge;

		void Reset()
		{
			DoCast(me, SPELL_BUFF_ATTACK_POWER);
			timer_buff_attack_power = 120 * IN_MILLISECONDS;
		}

		void EnterCombat(Unit* who) //chiamato solo 1 volta quando inizia il fight
		{
			me->MonsterYell("IL GRANDE GAMON TE LA FARA' PAGARE!!!", LANG_UNIVERSAL, 0);
			if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
			{
				DoCast(pTarget, SPELL_DEATH_GRIP);
				DoCast(pTarget, SPELL_DEATH_STRIKE);
			}
			timer_ali = 60 * IN_MILLISECONDS;
			timer_bolla = 90 * IN_MILLISECONDS;
			timer_cura_monaco = 30 * IN_MILLISECONDS;
			//timer_flame_shock = 10 * IN_MILLISECONDS;
			//timer_lava_burst = 13 * IN_MILLISECONDS;
			timer_fan = 17 * IN_MILLISECONDS;
			timer_cleave = 22 * IN_MILLISECONDS;
			timer_death_strike = 27 * IN_MILLISECONDS;
			timer_death_grip = 40 * IN_MILLISECONDS;
			timer_lampo_monaco = 33 * IN_MILLISECONDS;
			//timer_lava_surge = 13 * IN_MILLISECONDS;
		}

		void EnterEvadeMode()
		{

		}

		void ReceiveEmote(Player* /*player*/, uint32 uiTextEmote)
		{

		}

		void UpdateAI(uint32 uiDiff)
		{
			if (timer_buff_attack_power <= uiDiff)
			{
				DoCast(me, SPELL_BUFF_ATTACK_POWER);
			}
			else
				timer_buff_attack_power -= uiDiff;


			if (timer_ali <= uiDiff)
				DoCast(me, SPELL_ALI_PALADINO);
			else
				timer_ali -= uiDiff;

			if (timer_bolla <= uiDiff)
				DoCast(me, SPELL_BOLLA_PALADINO);
			else
				timer_bolla -= uiDiff;

			if (timer_cura_monaco <= uiDiff)
				DoCast(me, SPELL_CURA_MONACO);
			else
				timer_cura_monaco -= uiDiff;

			

			if (timer_fan <= uiDiff)
				DoCast(me, SPELL_FAN_OF_KNIVES);
			else
				timer_fan -= uiDiff;

			if (timer_cleave <= uiDiff)
				DoCastVictim(SPELL_CLEAVE_WARRIOR);
			else
				timer_cleave -= uiDiff;

			if (timer_lampo_monaco <= uiDiff)
				DoCastVictim(SPELL_LAMPO_MONACO);
			else
				timer_lampo_monaco -= uiDiff;



			if (me->GetHealthPct() <= 20)
			{
				if (timer_martello <= uiDiff)
				{
					DoCastVictim(SPELL_MARTELLO_PALADINO);
					timer_martello = 6 * IN_MILLISECONDS;
				}
				else
					timer_martello -= uiDiff;
			}


			DoMeleeAttackIfReady();
		}
	};
	CreatureAI* GetAI(Creature* creature) const
	{
		return new mob_gamonAI(creature);
	}
};



void AddSC_orgrimmar()
{
	new mob_gamon();
}
